from difflib import SequenceMatcher
from typing import Iterable

import attr


def __match_interface(text: str, comparisons: Iterable[str]) -> str:
    """
    A dummy function for describing the match function interface

    Arguments
    ---------
    text
        The string to match
    comparisons
        An interable of strings to match againsg

    Returns
    -------
    The best match
    """


def match_levenshtein_distance(
    query: str, comparisons: Iterable[str]
) -> "LevenshteinMatch":
    """
    Find the best match using Levenshtein distance
    """
    matches = []
    for comparison in comparisons:
        matches.append(
            LevenshteinMatch(
                query=query,
                target=comparison,
                distance=levenshtein_distance(query, comparison),
            )
        )

    return sorted(matches, key=lambda m: m.distance, reverse=True).pop()


def match_substring(query: str, comparisons: Iterable[str]) -> "SubstringMatch":
    """
    Find the best match using substring comparison
    """

    matches = []
    for comparison in comparisons:
        longest_match = SequenceMatcher(None, query, comparison).get_matching_blocks()[
            0
        ]
        match = SubstringMatch(query, comparison, longest_match[0], longest_match[-1])
        matches.append(match)
    top_match = sorted(matches, key=lambda m: m.length).pop()

    return top_match


def levenshtein_distance(s1, s2):
    """
    Calculate the Levenshtein distance between two strings

    From https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python
    """
    if len(s1) < len(s2):
        return levenshtein_distance(s2, s1)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = range(len(s2) + 1)
    for i, c1 in enumerate(s1):
        current_row = [i + 1]
        for j, c2 in enumerate(s2):
            insertions = (
                previous_row[j + 1] + 1
            )  # j+1 instead of j since previous_row and current_row are one character longer
            deletions = current_row[j] + 1  # than s2
            substitutions = previous_row[j] + (c1 != c2)
            current_row.append(min(insertions, deletions, substitutions))
        previous_row = current_row

    return previous_row[-1]


@attr.s
class Match:
    """
    Attributes
    ----------
    query
        The query string
    target
        The target string
    """

    query: str = attr.ib()
    target: str = attr.ib()


@attr.s
class SubstringMatch(Match):
    """
    Attributes
    ----------
    start_pos
        The position in the text where the match starts
    length
        The length of the match
    """

    start_pos: int = attr.ib()
    length: int = attr.ib()


@attr.s
class LevenshteinMatch(Match):
    """
    Attributes
    ----------
    distance
        The Levenshtein distance
    """

    distance: int = attr.ib()
