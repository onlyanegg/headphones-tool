from pathlib import Path

from django.apps import AppConfig


class HeadphonesConfig(AppConfig):
    name = "headphones"

    def ready(self):
        """
        Tell heroku-buildpack-nginx that the application is ready to receive traffic
        https://github.com/heroku/heroku-buildpack-nginx
        """
        Path("/tmp/app-initialized").touch()
