# Generated by Django 2.2.3 on 2019-09-02 08:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('headphones', '0003_posttime_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='headphonemodel',
            name='num_sold',
            field=models.IntegerField(null=True),
        ),
    ]
