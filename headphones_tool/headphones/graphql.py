import logging

import graphene
from graphene_django import DjangoObjectType

from .models import (ExchangeRates, HeadphoneModel, HeadphoneOffer,
                     Recommendation)
from .utils import convert_currency

log = logging.getLogger(__name__)


class HeadphoneModelType(DjangoObjectType):
    class Meta:
        model = HeadphoneModel


class HeadphoneOfferType(DjangoObjectType):
    class Meta:
        model = HeadphoneOffer


class RecommendationType(DjangoObjectType):
    class Meta:
        model = Recommendation


class Query(graphene.ObjectType):
    model = graphene.Field(HeadphoneModelType, id=graphene.ID())
    offer = graphene.Field(HeadphoneOfferType, id=graphene.ID())
    recommendation = graphene.Field(RecommendationType)

    models = graphene.List(
        HeadphoneModelType,
        first=graphene.Int(),
        skip=graphene.Int(),
        order_by=graphene.List(graphene.String),
        contains=graphene.String(),
    )
    offers = graphene.List(
        HeadphoneOfferType,
        models=graphene.List(graphene.ID),
        available=graphene.Boolean(),
        currency=graphene.String(),
    )
    recommendations = graphene.List(RecommendationType)

    def resolve_model(self, info, id):
        return HeadphoneModel.objects.get(pk=id)

    def resolve_offer(self, info, id):
        return HeadphoneOffer.objects.get(pk=id)

    def resolve_recommendation(self, info):
        return Recommendation.objects.all()

    def resolve_models(self, info, first=None, skip=None, order_by=None, contains=None):
        """
        Arguments
        ---------
        order_by
            A list of strings represnting model fields optionally prefixed with '-'
        """
        qs = HeadphoneModel.objects.all()
        if contains is not None:
            qs = qs.filter(name__contains=contains)
        if order_by is not None:
            qs = qs.order_by(*order_by)
        if skip is not None:
            qs = qs[skip:]
        if first is not None:
            qs = qs[:first]

        return qs

    def resolve_offers(self, info, models=None, available=None, currency=None):
        if models is not None and available is not None:
            offers = HeadphoneOffer.objects.filter(model__in=models).filter(
                available=available
            )
        elif models is not None:
            offers = HeadphoneOffer.objects.filter(model__in=models)
        elif available is not None:
            offers = HeadphoneOffer.objects.filter(available=available)
        else:
            offers = HeadphoneOffer.objects.all()

        if currency is not None:  # Convert currency
            rates = ExchangeRates.objects.last()
            if rates is None:
                log.warning(
                    "Exchange rates could not be fetched. Returning offers with original currency."
                )
                return offers

            for offer in offers:
                offer.cur_price = convert_currency(
                    offer.cur_price, offer.currency, currency, rates
                )
                offer.currency = currency

        return offers

    def resolve_recommendations(self, info):
        return Recommendation.objects.all()


schema = graphene.Schema(query=Query)
