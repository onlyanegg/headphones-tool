import json
from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from headphones.models import (HeadphoneBrand, HeadphoneModel, HeadphoneOffer,
                               PostTime, Recommendation)
from headphones.tasks import update_exchange_rates


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def add_arguments(self, parser):
        parser.add_argument(
            "-d",
            "--data",
            type=str,
            nargs="*",
            help="Type of data to initialize",
            choices=[
                "start_times",
                "brands",
                "models",
                "offers",
                "exchange_rates",
                "stats",
                "recommendations",
            ],
        )
        parser.add_argument(
            "-l",
            "--limit",
            type=int,
            help="A limit for the number of brands, models, offers, or recommendations to initialize",
        )

    def handle(self, *args, **options):
        """
        Save initial data to the database
        """

        self.stdout.write("Initializing data")
        if options.get("data"):
            if "start_times" in options["data"]:
                self._save_start_times()
            if "brands" in options["data"]:
                self._save_brands(options.get("limit"))
            if "models" in options["data"]:
                self._save_models(options.get("limit"))
            if "offers" in options["data"]:
                self._save_offers(options.get("limit"))
            if "exchange_rates" in options["data"]:
                self._save_exchange_rates()
            if "stats" in options["data"]:
                self._init_model_stats()
            if "recommendations" in options["data"]:
                self._init_recommendations(options.get("limit"))
        self.stdout.write("Finished initializing data")

    def _save_start_times(self):
        self.stdout.write(indent("Initializing previous crawl start times", 2))
        with open("./headphones/data/post_times.json", "r") as f:
            post_times_list = [json.loads(line) for line in f.readlines()]
        post_times = []
        for post_time in post_times_list:
            post_times.append(PostTime(**post_time))
        PostTime.objects.bulk_create(post_times)

    def _save_brands(self, limit=None):
        self.stdout.write(indent("Initializing headphone brands", 2))
        with open("./headphones/data/brands.json", "r") as f:
            brand_names = [json.loads(line)["name"] for line in f.readlines()]
        brands = []
        if limit is not None:
            brand_names = brand_names[:limit]
        for brand_name in brand_names:
            brands.append(HeadphoneBrand(name=brand_name))
        HeadphoneBrand.objects.bulk_create(brands)
        self.stdout.write(indent(f"Initialized {len(brands)} brands", 4))

    def _save_models(self, limit=None):
        self.stdout.write(indent("Initializing headphone models", 2))
        with open("./headphones/data/models.json", "r") as f:
            # A hacky way to get a unique list - maybe theres another datastructure for that
            # bulk_create fails if any of the names are not unique since that is a table requirement
            model_names = list(
                set([json.loads(line)["name"] for line in f.readlines()])
            )
        models = []
        if limit is not None:
            model_names = model_names[:limit]
        for model_name in model_names:
            model = HeadphoneModel(name=model_name)
            # model.set_brand()
            models.append(model)
        HeadphoneModel.objects.bulk_create(models)
        self.stdout.write(indent(f"Initialized {len(models)} models", 4))

    def _save_offers(self, limit=None):
        self.stdout.write(indent("Initializing headphone offers", 2))
        with open("./headphones/data/offers.json", "r") as f:
            offers_list = [json.loads(line) for line in f.readlines()]
        if limit is not None:
            offers_list = offers_list[:limit]
        for offer_dict in offers_list:
            offer = HeadphoneOffer(**offer_dict)
            offer.set_model()

    def _save_exchange_rates(self):
        self.stdout.write(indent("Initializing exchange rates", 2))
        update_exchange_rates()

    def _init_model_stats(self):
        self.stdout.write(indent("Initializing model statistics", 2))
        model_ids = (
            HeadphoneOffer.objects.all().values_list("model", flat=True).distinct()
        )
        for model in HeadphoneModel.objects.filter(id__in=model_ids):
            model.update_stats()

    def _init_recommendations(self, limit=None):
        self.stdout.write(indent("Initializing recommendations", 2))
        now = datetime.now()
        offers = HeadphoneOffer.objects.filter(
            available=True, post_timestamp__range=(now - timedelta(days=90), now)
        )
        if limit is not None:
            offers = offers[:limit]
        for offer in offers:
            if (
                offer.model.median_price is not None
                and offer.cur_price < offer.model.median_price
            ):
                Recommendation(offer=offer).save()


def indent(text, num_spaces):
    return f"{' ' * num_spaces}{text}"
