from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from headphones.models import (HeadphoneBrand, HeadphoneModel, HeadphoneOffer,
                               PostTime, Recommendation)


class Command(BaseCommand):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def add_arguments(self, parser):
        parser.add_argument(
            "-d",
            "--data",
            type=str,
            nargs="*",
            help="Type of data to delete",
            choices=[
                "all",
                "start_times",
                "brands",
                "models",
                "offers",
                "recommendations",
            ],
        )

    def handle(self, *args, **options):
        """
        Delete data from the database
        """

        self.stdout.write("Deleting data")
        data = options.get("data")
        if data:
            if "start_times" in data or "all" in data:
                self._delete_start_times()
            if "brands" in data or "all" in data:
                self._delete_brands(options.get("limit"))
            if "models" in data or "all" in data:
                self._delete_models(options.get("limit"))
            if "offers" in data or "all" in data:
                self._delete_offers(options.get("limit"))
            if "recommendations" in data or "all" in data:
                self._delete_recommendations(options.get("limit"))
        self.stdout.write("Finished deleting data")

    def _delete_start_times(self):
        self.stdout.write(indent("Deleting previous crawl start times", 2))
        PostTime.objects.all().delete()

    def _delete_brands(self, limit=None):
        self.stdout.write(indent("Deleting headphone brands", 2))
        HeadphoneBrand.objects.all().delete()

    def _delete_models(self, limit=None):
        self.stdout.write(indent("Deleting headphone models", 2))
        HeadphoneModel.objects.all().delete()

    def _delete_offers(self, limit=None):
        self.stdout.write(indent("Deleting headphone offers", 2))
        HeadphoneOffer.objects.all().delete()

    def _delete_recommendations(self, limit=None):
        self.stdout.write(indent("Deleting recommendations", 2))
        Recommendation.objects.all().delete()


def indent(text, num_spaces):
    return f"{' ' * num_spaces}{text}"
