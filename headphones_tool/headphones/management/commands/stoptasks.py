"""
A management command to start Celery scheduled tasks
"""

from django.core.management.base import BaseCommand

from django_celery_beat.models import IntervalSchedule, PeriodicTask


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Stopping scheduled tasks")
        PeriodicTask.objects.get(task="headphones.tasks.crawl_offers").delete()
        PeriodicTask.objects.get(task="headphones.tasks.crawl_models").delete()
        PeriodicTask.objects.get(task="headphones.tasks.send_recommendations").delete()
