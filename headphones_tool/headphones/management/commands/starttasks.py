"""
A management command to start Celery scheduled tasks
"""

from django.core.management.base import BaseCommand
from django_celery_beat.models import IntervalSchedule, PeriodicTask


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.stdout.write("Starting scheduled tasks")
        test_schedule, _ = IntervalSchedule.objects.get_or_create(
            every=1, period=IntervalSchedule.MINUTES
        )
        five_minute_schedule, _ = IntervalSchedule.objects.get_or_create(
            every=5, period=IntervalSchedule.MINUTES
        )
        daily_schedule, _ = IntervalSchedule.objects.get_or_create(
            every=1, period=IntervalSchedule.DAYS
        )

        PeriodicTask.objects.create(
            name="Crawl Offers",
            task="headphones.tasks.crawl_offers",
            interval=five_minute_schedule,
        )
        PeriodicTask.objects.create(
            name="Crawl Models",
            task="headphones.tasks.crawl_models",
            interval=daily_schedule,
        )
        PeriodicTask.objects.create(
            name="Send Recommendations",
            task="headphones.tasks.send_recommendations",
            interval=daily_schedule,
        )
        PeriodicTask.objects.create(
            name="Update Exchange Rates",
            task="headphones.tasks.update_exchange_rates",
            interval=daily_schedule,
        )
