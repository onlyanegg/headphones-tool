"""
A management command to run an initial scrape of the data and store it locally
"""

from django.core.management.base import BaseCommand
from scrapy.crawler import CrawlerProcess

from headphones.spiders import ModelSpider, OfferSpider


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument(
            "spider", type=str, choices=["model", "offer"], help="Spider to run"
        )
        parser.add_argument("-p", "--pages", type=int, help="Number of pages to crawl")

    def handle(self, *args, **options):
        """
        Save initial data to the database
        """

        process = CrawlerProcess(
            {"ITEM_PIPELINES": {"headphones.spiders.SaveToFilesystem": 300}}
        )
        if options["spider"] == "model":
            process.crawl(ModelSpider, pages=options.get("pages"), full_crawl=True)
        else:
            process.crawl(OfferSpider, pages=options.get("pages"), full_crawl=True)
        process.start()
