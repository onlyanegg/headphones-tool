import logging

from django.db import models

from .similarity import match_levenshtein_distance, match_substring
from .utils import get_word_index, to_usd

log = logging.getLogger(__name__)

# TODO: Create a PriceField
#   Would be a float with precision (maybe this is decimal field)


class HeadphoneBrand(models.Model):
    name = models.CharField(max_length=300, unique=True)


class HeadphoneModel(models.Model):
    """
    Attributes
    ----------
    prices
        A set of prices the model has sold for
    """

    name = models.CharField(max_length=300, unique=True)
    # on_delete should ideally call set_brand
    brand = models.ForeignKey(HeadphoneBrand, null=True, on_delete=models.SET_NULL)
    avg_price = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    median_price = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    std_dev = models.FloatField(null=True)
    num_sold = models.IntegerField(null=True)

    def set_brand(self):
        """
        """
        brands = HeadphoneBrand.objects.all()
        brand_name = match_levenshtein_distance(
            " ".join(self.name.split()[:3]), brands.values_list("name", flat=True)
        ).target
        self.brand = brands.get(name=brand_name)
        self.save()

    def update_stats(self):
        """
        Update price statistics for offers with this model

        NOTE:
        All stats are stored in USD
        """
        offers = HeadphoneOffer.objects.filter(model=self).filter(available=False)

        rates = ExchangeRates.objects.last()
        if rates is None:
            log.error("Exchange rates have not been fetched. Skipping update_stats().")
            return

        prices = sorted(
            to_usd(offer.cur_price, offer.currency, rates) for offer in offers
        )
        if len(prices):
            self.median_price = prices[len(prices) // 2]
            self.avg_price = sum(prices) / len(offers)

        self.save()

    def update_num_sold(self):
        self.num_sold = HeadphoneOffer.objects.filter(
            model=self, available=False
        ).count()
        self.save()


class HeadphoneOffer(models.Model):
    """
    Attributes
    ----------
    aliases
        A set of strings that are similar to and might match better than the posted title
    price_history
        A list of prices this has been offered at
    ship_to
        A set of locations available for shipping
    """

    url = models.URLField(max_length=400, unique=True)
    title = models.CharField(max_length=300)
    norm_title = models.CharField(max_length=300)
    model = models.ForeignKey(HeadphoneModel, on_delete=models.CASCADE)
    brand = models.ForeignKey(HeadphoneBrand, null=True, on_delete=models.SET_NULL)
    currency = models.CharField(max_length=10)
    cur_price = models.DecimalField(max_digits=10, decimal_places=2)
    available = models.BooleanField()
    post_timestamp = models.DateTimeField()
    sale_timestamp = models.DateTimeField(null=True)
    user = models.CharField(max_length=200)

    def set_model(self):
        """
        Set the model
        """

        models = HeadphoneModel.objects.all()
        match = match_substring(self.norm_title, models.values_list("name", flat=True))
        self.model = models.get(name=match.target)
        self.save()

        return self.model

    def set_model_(self):
        """
        Find the brand and model

        Find the brand
            Use substring matching to find the most likely brand from the normalized title and the
            list of known brands
        Find the model
            Use Levenshtein distance to find the most likely model from a subset of the normalized
            title and the list of models of the defined brand
        """

        # Find the brand
        brands = HeadphoneBrand.objects.all()
        brand_match = match_substring(
            self.norm_title, brands.values_list("name", flat=True)
        )
        self.brand = HeadphoneBrand(name=brand_match.target)

        # Find the model
        models = HeadphoneModel.objects.filter(brand=self.brand)
        brand_index = get_word_index(self.norm_title, brand_match.start_pos)
        match_string = " ".join(self.norm_title.split()[brand_index : brand_index + 3])
        model_match = match_levenshtein_distance(
            match_string, models.values_list("name", flat=True)
        )
        self.model = HeadphoneModel(name=model_match.match)

        # Save and return
        self.save()
        return self.model


class Recommendation(models.Model):
    offer = models.ForeignKey("HeadphoneOffer", on_delete=models.CASCADE)


class SingletonModel(models.Model):
    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.__class__.objects.exclude(id=self.id).delete()
        super().save(*args, **kwargs)

    @classmethod
    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()


class PostTime(SingletonModel):
    spider = models.CharField(max_length=100)
    time = models.DateTimeField(null=True)


class ExchangeRates(SingletonModel):
    """
    Current exchange rates with target USD 
    """

    eur = models.DecimalField(decimal_places=10, max_digits=20)
    gbp = models.DecimalField(decimal_places=10, max_digits=20)
    aud = models.DecimalField(decimal_places=10, max_digits=20)
