import logging
import math
from datetime import datetime, timedelta, timezone
from os.path import basename, join
from urllib.parse import parse_qs, urlparse, urlunparse

from django.db.utils import IntegrityError, OperationalError
from scrapy import Spider
from scrapy.exporters import JsonLinesItemExporter
from scrapy_djangoitem import DjangoItem

from .constants import Currencies
from .models import HeadphoneModel, HeadphoneOffer, PostTime, Recommendation
from .utils import normalize_model

SCHEME = "https"
NETLOC = "www.head-fi.org"
OUTPUT_FILE = "offers.txt"
SALE_TYPES = set(["for sale or trade", "for sale"])
SHIP_TO_TYPES = set(["anywhere", "united states", "north america"])

log = logging.getLogger(__name__)

currency_map = {"euro": Currencies.eur}


class OfferSpider(Spider):
    name = "offer-spider"
    start_urls = ["https://www.head-fi.org/forums/headphones-for-sale-trade.6550"]

    def __init__(self, *args, pages=None, full_crawl=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._pages = pages
        self._full_crawl = full_crawl
        self._cur_crawl_start = None

        try:
            self._prev_crawl_start = PostTime.objects.get(spider=self.name).time
        except (PostTime.DoesNotExist, OperationalError):
            self._prev_crawl_start = datetime.utcfromtimestamp(0)

    def parse(self, response):
        """
        Parse offer list
        """

        for item in response.css(".discussionListItem"):
            # Ignore sticky posts
            if "sticky" in item.attrib["class"]:
                continue

            # Get the datetime of the latest post in the thread
            #
            # The abbr element contains the posix epoch timestamp, but it can be found in the HTML
            # only for a week or so. Try to get the posix epoch, and fall back to getting a text
            # representation of the datetime.
            #
            # Note that the text representations appear to be in US Eastern Time
            try:
                post_time = datetime.utcfromtimestamp(
                    int(item.css(".lastPost").css("abbr").pop().attrib["data-time"])
                )
            except IndexError:
                try:
                    post_time = get_datetime_from_string(
                        item.css(".lastPost").css(".DateTime").attrib["title"]
                    )
                except KeyError:
                    # Entries that don't have the DateTime span with a title attribute may be
                    # corrupt, so skip them altogether
                    continue

            # Save current crawl start time
            if self._cur_crawl_start is None:
                self._cur_crawl_start = post_time
                yield PostTimeItem(time=post_time, spider=self.name)

            # Stop the spider if we viewed the current post during the previous crawl
            if not self._full_crawl and post_time <= self._prev_crawl_start:
                log.info(
                    "Arrived at start of previous crawl. Stopping. Post time: %s",
                    post_time,
                )
                return

            # Parse offer
            offer = HeadphoneOfferItem()
            offer["post_timestamp"] = post_time

            post_type = ""
            second_row = item.css(".secondRow")
            for dl in second_row.css(".pairsInline").css("dl"):
                dt = dl.css("dt").xpath("text()").get().lower().rstrip(" :")
                dd = dl.css("dd").xpath("text()").get().lower().strip(" \t\n")
                if dt == "type":
                    post_type = dd
                elif dt == "currency":
                    offer["currency"] = currency_map.get(dd, dd)
                elif dt == "price":
                    offer["cur_price"] = dd
                # Ignore 'ship to' for MVP
                # elif dt == "ship to":
                #    for loc in (
                #        dl.css("dd").css("ul").css("li").xpath("text()").getall()
                #    ):
                #        offer.ship_to.add(loc.lower().strip())

            # Ignore non-sale posts
            if post_type not in SALE_TYPES:
                continue

            title_anchor = item.css(".title").css("a")
            title = title_anchor.xpath("text()").get()
            offer["title"] = title
            offer["norm_title"] = normalize_model(title)
            offer["url"] = urlunparse(
                (SCHEME, NETLOC, title_anchor.attrib["href"], "", "", "")
            )

            offer["user"] = second_row.css(".username").xpath("text()").get()

            offer["available"] = True
            if "sold" in offer["norm_title"]:
                offer["available"] = False
                # TODO: Sale timestamp can be updated if new messages are added to the thread. This
                #       should not happen.
                offer["sale_timestamp"] = post_time

            yield offer

        # Follow the next page
        page = 1
        basename_ = basename(urlparse(response.request.url).path)
        if "page-" in basename_:
            page = int(basename_.split("-")[-1])
        if self._pages is None or page <= self._pages:
            yield response.follow(
                response.css("a").css(".hfxt_pagination_right").pop().attrib["href"],
                self.parse,
            )


class ModelSpider(Spider):
    name = "model-spider"
    start_urls = ["https://www.head-fi.org/showcase/category/headphones.258/"]

    def __init__(self, *args, pages=None, full_crawl=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._pages = pages
        self._full_crawl = full_crawl
        self._cur_crawl_start = None

        try:
            self._prev_crawl_start = PostTime.objects.get(spider=self.name).time
        except (PostTime.DoesNotExist, OperationalError):
            self._prev_crawl_start = 0

    def parse(self, response):
        """
        Add post titles to a database
        Follow next link
        """

        for item in response.css(".showcaseList").css(".showcaseItem"):
            model = HeadphoneModelItem()
            model["name"] = normalize_model(
                item.css(".main").css(".title").css("a").xpath("text()").get()
            )

            # Stop the spider if we viewed the current post during the previous crawl
            try:
                post_time = int(
                    item.css(".main")
                    .css(".itemDetails")
                    .css("abbr")
                    .pop()
                    .attrib["data-time"]
                )
            except IndexError:
                pass
            else:
                if self._cur_crawl_start is None:
                    self._cur_crawl_start = post_time
                    yield PostTimeItem(time=post_time, spider=self.name)
                if not self._full_crawl and post_time <= self._prev_crawl_start:
                    log.info(
                        "Arrived at start of previous crawl. Stopping. Post time: %s",
                        post_time,
                    )
                    return

            yield model

        # Follow next page
        page = int(
            parse_qs(urlparse(response.request.url).query).get("page", ["1"]).pop()
        )
        if self._pages is None or page <= self._pages:
            yield response.follow(
                response.css("a").css(".hfxt_pagination_right").pop().attrib["href"],
                self.parse,
            )


class HeadphoneModelItem(DjangoItem):
    django_model = HeadphoneModel


class HeadphoneOfferItem(DjangoItem):
    django_model = HeadphoneOffer


class PostTimeItem(DjangoItem):
    django_model = PostTime


class SaveToFilesystem:
    _data_dir = "./headphones/data"

    def open_spider(self, spider):
        """
        """
        self._offers_file = open(join(self._data_dir, "offers.json"), "wb")
        self._models_file = open(join(self._data_dir, "models.json"), "wb")
        self._post_times_file = open(join(self._data_dir, "post_times.json"), "ab")

        self._offer_exporter = JsonLinesItemExporter(self._offers_file)
        self._model_exporter = JsonLinesItemExporter(self._models_file)
        self._post_time_exporter = JsonLinesItemExporter(self._post_times_file)

    def close_spider(self, spider):
        """
        """
        self._offers_file.close()
        self._models_file.close()
        self._post_times_file.close()

    def process_item(self, item, spider):
        """
        """
        if isinstance(item, HeadphoneOfferItem):
            self._process_offer(item, spider)
        elif isinstance(item, HeadphoneModelItem):
            self._process_model(item, spider)
        elif isinstance(item, PostTimeItem):
            self._process_post_time(item, spider)

        return item

    def _process_offer(self, item, spider):
        self._offer_exporter.export_item(item)

    def _process_model(self, item, spider):
        self._model_exporter.export_item(item)

    def _process_post_time(self, item, spider):
        self._post_time_exporter.export_item(item)


class SaveToDatabase:
    # Don't need this with a singleton model
    #
    # def close_spider(self, spider):
    #    """
    #    Clean PostTime items
    #    """

    #    for spider in set(item.spider for item in PostTime.objects.all()):
    #        items = PostTime.objects.filter(spider=spider).order_by("-time")[1:]
    #        PostTime.objects.filter(pk__in=items).delete()

    def process_item(self, item, spider):
        if isinstance(item, HeadphoneOfferItem):
            self._process_offer(item, spider)
        elif isinstance(item, HeadphoneModelItem):
            self._process_model(item, spider)
        elif isinstance(item, PostTimeItem):
            self._process_post_time(item, spider)

        return item

    def _process_offer(self, item, spider):
        """
        """
        offer, created = HeadphoneOffer.objects.update_or_create(**item)
        if created:
            offer.set_model()

        # This will update stats too often. Many times, posts are just bumped with no change in
        # price.
        offer.model.update_stats()

    def _process_model(self, item, spider):
        """
        """
        try:
            item.save()
        except IntegrityError:
            pass

    def _process_post_time(self, item, spider):
        item.save()


class CreateRecommendation:
    def process_item(self, item, spider):
        """
        """
        if isinstance(item, HeadphoneModelItem):
            self._process_offer(item, spider)

        return item

    def _process_offer(self, item, spider):
        offer = item.save(commit=False)
        if offer.cur_price < offer.model.avg_price:
            Recommendation(offer=offer).save()


def get_datetime_from_string(text):
    """
    Convert string to datetime instance and add timezone info (EST)
    """

    ret = datetime.strptime(text, "%b %d, %Y at %I:%M %p")
    ret = ret.replace(tzinfo=timezone(-timedelta(hours=4)))
    return ret
