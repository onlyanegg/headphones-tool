import typing
from decimal import Decimal

from slugify import slugify

from .constants import Currencies

if typing.TYPE_CHECKING:
    from .models import ExchangeRates


def normalize_model(text):
    """
    Normalize the model name
    """

    # TODO: convert number strings to numbers?
    stop_words = [
        "cable",
        "case",
        "like",
        "new",
        "headphones",  # do I need both of these? Can I do it another way?
        "headphone",
        "earbuds",
        "earphones",
        "for",
        "sale",
        "a",
        "of",
        "and",
        "the",
        "in",
        "condition",
        "excellent",
        "fs",  # for sale
        "w",  # with
        "with",
        "price",
        "trade",
    ]
    return slugify(text, stopwords=stop_words, separator=" ")


# def get_brands():
#    """
#    Notes for getting brands from the models.json. NOT A WORKING FUNCTION.
#
#    Get the most common 1, 2, or 3 word phrases from the beginning of the model name
#    """
#    with open("./data/models.json") as f:
#        models = [json.loads(line) for line in f.readlines()]
#    split_models = [model["name"].split() for model in models]
#    for model in split_models:
#        for i in range(1, 4):
#            substrings.append(" ".join(model[:i]))
#
#    counter = Counter(split_models)
#    counter.most_common(20)


def get_word_index(text, index):
    """
    Get the index of the word which corresponds to the index of that word in the string

    Assumes text has been normalized using normalize_model
    """

    split_text = text.split()

    x = 0
    for i, word in enumerate(split_text):
        for char in word:
            if x == index:
                return i
            x += 1
        x += 1  # account for single spaces in text

    return None


# TODO: Fix this for float * decimal multiplication
def convert_currency(
    price: Decimal, source: str, target: str, rates: "ExchangeRates"
) -> Decimal:
    """
    Helper function to convert currency
    """

    if source == target:
        return price

    price_usd = getattr(rates, source) * price
    if target == Currencies.usd:
        price_target = price_usd
    else:
        price_target = 1 / getattr(rates, target) * price_usd

    return price_target.quantize(Decimal(".01"))


def to_usd(price, currency, rates):
    return convert_currency(price, currency, Currencies.usd, rates)


def to_eur(price, currency, rates):
    return convert_currency(price, currency, Currencies.eur, rates)


def to_gbp(price, currency, rates):
    return convert_currency(price, currency, Currencies.gbp, rates)


def to_aud(price, currency, rates):
    return convert_currency(price, currency, Currencies.aud, rates)
