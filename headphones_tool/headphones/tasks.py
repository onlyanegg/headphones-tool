import logging

import requests
from celery.decorators import task
from django.conf import settings
from django.contrib.auth.models import User
from scrapy.crawler import CrawlerProcess

from .models import ExchangeRates, Recommendation
from .spiders import ModelSpider, OfferSpider

log = logging.getLogger(__name__)


@task
def crawl_offers():
    process = CrawlerProcess(
        {
            "ITEM_PIPELINES": {
                "headphones.spiders.SaveToDatabase": 300,
                "headphones.spiders.CreateRecommendation": 400,
            }
        }
    )
    process.crawl(OfferSpider)
    process.start(stop_after_crawl=False)


@task
def crawl_models():
    process = CrawlerProcess(
        {"ITEM_PIPELINES": {"headphones.spiders.SaveToDatabase": 300}}
    )
    process.crawl(ModelSpider)
    process.start(stop_after_crawl=False)


@task
def send_recommendations():
    """
    """

    recommendations = Recommendation.objects.all()
    users = User.objects.all()

    text = (
        "Here are your recommendations for today.\n"
        "\n"
        "\n".join([r.offer.url for r in recommendations])
    )
    for user in users:
        requests.post(
            f"https://api.mailgun.net/v3/{settings.MAILGUN_DOMAIN}/messages",
            auth=("api", settings.MAILGUN_API_KEY),
            data={
                "from": f"<no-reply@{settings.MAILGUN_DOMAIN}>",
                "to": [user.email],
                "subject": "Headphone recommendations",
                "text": text,
            },
        )


@task
def clean_recommendations():
    """
    Remove recommendations from the recommendations table if they no longer match the criteria
    """


@task
def update_exchange_rates():
    """
    Task to update exchange rates
    """

    resp = requests.get(
        "https://api.exchangeratesapi.io/latest", params={"base": "USD"}
    )

    if resp.status_code != 200:
        log.warning("Failed to fetch latest exchange rates")
        return

    rates = resp.json()["rates"]
    ExchangeRates(
        eur=str(1 / rates["EUR"]), gbp=str(1 / rates["GBP"]), aud=str(1 / rates["AUD"])
    ).save()
